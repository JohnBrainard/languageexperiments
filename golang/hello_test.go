package golang

import (
	"github.com/google/go-cmp/cmp"
	"testing"
)

func TestHello(t *testing.T) {
	t.Run("Hello() returns correct value", func(t *testing.T) {
		result := Hello()
		expected := "Hello, Experiments!"

		if diff := cmp.Diff(expected, result); diff != "" {
			t.Errorf("Hello() = %s, want %s\n%s", result, expected, diff)
		}
	})
}
