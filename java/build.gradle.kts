plugins {
	java
}

group = "com.fretboardstudio.experiments"

repositories {
	mavenCentral()
}

dependencies {
	testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.0")
}

tasks.withType<Test> {
	useJUnitPlatform {
		includeEngines("junit-jupiter")
	}
}

configure<JavaPluginConvention> {
	sourceCompatibility = JavaVersion.VERSION_13
}