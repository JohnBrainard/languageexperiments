package com.fretboardstudio.experiments.java;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelloExperimentsTest {
	private HelloExperiments experiments;

	@BeforeEach
	public void setup() {
		experiments = new HelloExperiments();
	}

	@Test
	public void testCreateMessage() {
		String result = experiments.createMessage();
		String expected = "Hello, Experiments";

		assertEquals(expected, result);
	}
}