import org.jetbrains.kotlin.gradle.plugin.mpp.KotlinNativeCompilation

plugins {
	kotlin("multiplatform") version "1.3.61"
}

repositories {
	mavenCentral()
}

val nativeCompilations: List<KotlinNativeCompilation> by project.extra

kotlin {
	jvm() // Create a JVM target with the default name 'jvm'
	js()  // JS target named 'js'

	// Windows (MinGW X64) target named 'mingw'
	mingwX64("mingw") {
		binaries {
			executable {
				entryPoint = "sample.win32.main"
				linkerOpts("-Wl,--subsystem,windows")
			}
		}
	}

	sourceSets {
		val commonMain by getting {
			dependencies {
				implementation("org.jetbrains.kotlin:kotlin-stdlib-common")
				implementation(kotlin("test-common"))
				implementation(kotlin("test-annotations-common"))
			}
		}

		// Default source set for JVM-specific sources and dependencies:
		jvm().compilations["main"].defaultSourceSet {
			dependencies {
				implementation(kotlin("stdlib-jdk8"))
			}
		}
		jvm().compilations["test"].defaultSourceSet {
			dependencies {
				implementation(kotlin("test-junit"))
			}
		}
	}
}