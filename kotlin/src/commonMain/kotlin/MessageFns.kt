package sample.common

fun createMessage(message: String): String {
	return "Common Message: $message"
}

fun createMessage(factoryFn:()->String):String{
	val result = StringBuilder()
	result.append("Common Message:").append(factoryFn())
	return result.toString()
}
