import sample.common.createMessage
import kotlin.test.Test
import kotlin.test.asserter

internal class MessageFnsKtTest {
	@Test
	fun testCreateMessage() {
		val result = createMessage("hello-test")
		val expected = "Common Message: hello-test"
		asserter.assertEquals("message result", expected, result)
	}
}
