package sample.win32

import kotlinx.cinterop.*
import platform.windows.*
import sample.common.createMessage

fun main() {
	val message = StringBuilder()
	memScoped {
		val buffer = allocArray<UShortVar>(MAX_PATH)
		GetModuleFileNameW(null, buffer, MAX_PATH)

//		val path = buffer.toString()
//				.split("\\")
//				.dropLast(1)
//				.joinToString("\\")

		message.append(createMessage {
			buffer.toString()
					.split("\\")
					.dropLast(1)
					.joinToString("\\")
		})

//		message.append(createMessage("hello, foo!"))
//		message.append(CreateMessage(path))
//		message.append("Message From Path: $path\n")
	}

	MessageBoxW(null, "Hello:\nHello World:\n$message",
			"From Kotlin Native",
			(MB_YESNOCANCEL or MB_ICONQUESTION).convert())
}

