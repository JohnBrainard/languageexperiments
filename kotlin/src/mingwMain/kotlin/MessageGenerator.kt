package sample.win32

class MessageGenerator(
		private val prefix: String? = null,
		private val suffix: String? = null) {

	fun newMessage(message: String) = buildString {
		prefix?.also { append(prefix).append(" ") }
		append(message)
		suffix?.also { append(" ").append(suffix) }
	}
}
