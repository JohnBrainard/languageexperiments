package sample.win32

import kotlin.test.Test
import kotlin.test.assertEquals

internal class MessageGeneratorTest {

	@Test
	fun newMessage() {
		val generator = MessageGenerator(prefix = "(", suffix = ")")

		val result: String = generator.newMessage("hello")
		val expected = "( hello )"

		assertEquals(expected, result, "message generated correctly")
	}
}
